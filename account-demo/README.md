# Introduction

The Account application was developed to provide simple internal account transfer function. 

# Running The Application

To test the example application run the following commands.

* To setup the h2 database run.

        java -jar target/account-demo-1.0-SNAPSHOT.jar db migrate setup.yml

* To run the server run.

        java -jar target/account-demo-1.0-SNAPSHOT.jar server setup.yml

* To see a list of accounts

	GET http://localhost:8080/accounts

* To see an individual account

	GET http://localhost:8080/accounts/{accountId}

* To post a new transfer.

	POST http://localhost:8080/accounts
	e.g.
	{
	"fromAccountId": 1,
	"toAccountId": 2,
	"amount": 20
	}
